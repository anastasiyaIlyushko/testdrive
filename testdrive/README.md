рабочая директория `testdrive`

шаблон лежит в `testdrive/themes`

скрипт парсера в `testdrive/protected/commands/LogParserCommand.php`
запуск 
```bash
cd protected
./yiic help logparser

./yiic logparser data/nginx.log
or
./yiic logparser data/nginx.log '/(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<time>\d{2}\/[a-zA-Z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] \"(?P<method>(GET|POST)) (?P<request>.+) (?P<protocol>HTTP\/\d\.\d)\" (?P<status>\d{3}) (?P<size>\d+) (["](?P<referer>(\-)|(.+))["]) (["](?P<user_agent>.+)["]) (["](?P<forwarded_for>.+)["])/'
```

пример лога `testdrive/protected/data/nginx.log`

конфиг приложения в `testdrive/protected/config`

расширение bootstrap`testdrive/protected/extensions/bootstrap`

миграции для базы `testdrive/protected/migrations`

запускать миграции ```shell ./yiic migrate```

в базе лежит один юзер admin/admin

к api стучаться `GET /index.php?r=log/api`

для авторизации необходимо присылать заголовок `authorization: Basic YWRtaW46YWRtaW4=` (соответствует admin/admin в бд)

необходимо слать заголовок `Content-Type: application/json`

параметры фильтрации:

Log[ip]: строка

Log[date_first]: строка в формате (mm/dd/yyyy);пример: 04/06/2020

Log[date_last]: строка в формате (mm/dd/yyyy);пример: 04/07/2020

Log[method]: строка

Log[request]: строка

Log[protocol]: строка

Log[status]: число

Log[size]: число

Log[referer]: строка

Log[user_agent]: строка

Log[forwarded_for]: строка

пример:
```bash
curl -X GET -H "Content-Type: application/json" 
\ -H "Authorization: Basic YWRtaW46YWRtaW4=" 
\ 'http://localhost:8082/index.php?r=log%2Fapi&Log%5Btime%5D=04%2F06%2F2020+-+04%2F07%2F2020' | jq .

```
