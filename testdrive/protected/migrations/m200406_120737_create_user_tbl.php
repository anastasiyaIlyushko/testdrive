<?php

class m200406_120737_create_user_tbl extends CDbMigration
{
	public function up()
	{
        $this->createTable('tbl_user', array(
            'id' => 'pk',
            'username' => 'VARCHAR(128) NOT NULL',
            'password' => 'VARCHAR(128) NOT NULL',
            'email' => 'VARCHAR(128) NOT NULL',
        ));
    }

	public function down()
	{
		echo "m200406_120737_create_user_tbl does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}