<?php

class m200406_121532_insert_user_admin extends CDbMigration
{
	public function up()
	{
        $this->insert('tbl_user', array(
            'username' => 'admin',
            'password' => 'admin',
            'email' => 'admin@admin.ru',
        ));
	}

	public function down()
	{
		echo "m200406_121532_insert_user_admin does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}