<?php

class m200406_152509_create_log_tbl extends CDbMigration
{
    public function up()
    {
        $this->createTable('tbl_log', array(
            'id' => 'pk',
            'ip' => 'VARCHAR(32) NOT NULL',
            'time' => 'TIMESTAMP NOT NULL',
            'method' => 'VARCHAR(8) NOT NULL',
            'request' => 'VARCHAR(512) NOT NULL',
            'protocol' => 'VARCHAR(16) NOT NULL',
            'status' => 'INT UNSIGNED NOT NULL',
            'size' => 'INT UNSIGNED NOT NULL',
            'referer' => 'VARCHAR(512) NOT NULL',
            'user_agent' => 'VARCHAR(512) NOT NULL',
            'forwarded_for' => 'VARCHAR(512) NOT NULL',
        ));
    }

    public function down()
    {
        echo "m200406_152509_create_log_tbl does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}