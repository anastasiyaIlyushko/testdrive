<?php
/**
 * Created by PhpStorm.
 * User: nastya
 * Date: 06.04.20
 * Time: 18:14
 */

require_once "models/Log.php";

class LogParserCommand extends CConsoleCommand
{

    public function getHelp()
    {
        return <<<EOD
USAGE
  logparser <file-path> [mask]

DESCRIPTION
  This command parse nginx logs for the next fields (regexp group's name):
  
  ip
  time
  method
  request
  protocol
  status
  size
  referer
  user_agent
  forwarded_for
  
  

PARAMETERS
 * file-path: required, path of file with logs.
 * mask: regexp mask for every row. Default: '/(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<time>\d{2}\/[a-zA-Z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] \"(?P<method>(GET|POST)) (?P<request>.+) (?P<protocol>HTTP\/\d\.\d)\" (?P<status>\d{3}) (?P<size>\d+) (["](?P<referer>(\-)|(.+))["]) (["](?P<user_agent>.+)["]) (["](?P<forwarded_for>.+)["])/'

EXAMPLES
 * Parse:
        /tmp/nginx.log

EOD;
    }

    /**
     * Execute the action.
     * @param array $args command line parameters specific for this command
     * @return integer|null non zero application exit code for help or null on success
     */
    public function run($args)
    {
        if (!isset($args[0])) {
            echo "Error: file's path name is required.\n";
            echo $this->getHelp();
            return 1;
        }

        $mask = '/(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) - - \[(?P<time>\d{2}\/[a-zA-Z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] \"(?P<method>(GET|POST)) (?P<request>.+) (?P<protocol>HTTP\/\d\.\d)\" (?P<status>\d{3}) (?P<size>\d+) (["](?P<referer>(\-)|(.+))["]) (["](?P<user_agent>.+)["]) (["](?P<forwarded_for>.+)["])/';
        if (isset($args[1])) {
            $mask = $args[1];
        }

        try {
            $this->parse($args[0], $mask);
        } catch (Exception $e) {
            echo "Error: {$e->getMessage()}.\n";
        }

    }

    protected function parse(string $filePath, string $mask)
    {
        $handle = fopen($filePath, "r");
        if (!$handle) {
            throw new CException("Error: open file '{$filePath}'");
        }

        while (($str = fgets($handle)) !== false) {
            preg_match($mask, $str, $matches);

            $model = new Log();
            $matches["time"] = date("Y-m-d H:i:s", strtotime($matches["time"]));
            $model->attributes = $matches;
            if (!$model->save()) {
                throw new CException("Error: save data '{$matches}'");
            }
        }

        fclose($handle);
    }
}