<?php
$this->breadcrumbs=array(
	'Index',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('log-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Logs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php
$dateisOn = $dateisOn = $this->widget('zii.widgets.jui.CJuiDatePicker', array(

        // 'model'=>$model,

        'name' => 'Log[date_first]',

        'language' => 'id',

        'value' => $model->date_first,

        // additional javascript options for the date picker plugin

        'options'=>array(

            'showAnim'=>'fold',

            'dateFormat'=>'mm/dd/yy',

            'changeMonth' => 'true',

            'changeYear'=>'true',

            'constrainInput' => 'false',

        ),

        'htmlOptions'=>array(

            'style'=>'height:20px;width:70px;',

        ),

// DONT FORGET TO ADD TRUE this will create the datepicker return as string

    ),true) . '<br> To <br> ' . $this->widget('zii.widgets.jui.CJuiDatePicker', array(

        // 'model'=>$model,

        'name' => 'Log[date_last]',

        'language' => 'id',

        'value' => $model->date_last,

        // additional javascript options for the date picker plugin

        'options'=>array(

            'showAnim'=>'fold',

            'dateFormat'=>'mm/dd/yy',

            'changeMonth' => 'true',

            'changeYear'=>'true',

            'constrainInput' => 'false',

        ),

        'htmlOptions'=>array(

            'style'=>'height:20px;width:70px',

        ),

// DONT FORGET TO ADD TRUE this will create the datepicker return as string

    ),true);


?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'log-grid',
	'dataProvider'=>$model->search(),
    'afterAjaxUpdate'=>"function() {

						jQuery('#Log_date_first').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'mm/dd/yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));

						jQuery('#Log_date_last').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'mm/dd/yy','changeMonth':'true','showButtonPanel':'true','changeYear':'true','constrainInput':'false'}));

						}",
	'filter'=>$model,
	'columns'=>array(
		'ip',
		array(
            'name'=>'time',
            'type'=>'datetime',
            'filter' =>$dateisOn// '<input class="datepicker" type="text" name="Log[time]">'
        ),
		'method',
		'request',
		'protocol',
		'status',
		'size',
		'referer',
		'user_agent',
		'forwarded_for',
	),
)); ?>
