<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<?php echo $form->textFieldRow($model,'ip',array('class'=>'span5','maxlength'=>32)); ?>

	<?php echo $form->textFieldRow($model,'time',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'method',array('class'=>'span5','maxlength'=>8)); ?>

	<?php echo $form->textFieldRow($model,'request',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'protocol',array('class'=>'span5','maxlength'=>16)); ?>

	<?php echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'size',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'referer',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'user_agent',array('class'=>'span5','maxlength'=>512)); ?>

	<?php echo $form->textFieldRow($model,'forwarded_for',array('class'=>'span5','maxlength'=>512)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
