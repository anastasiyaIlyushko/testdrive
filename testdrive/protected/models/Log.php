<?php

/**
 * This is the model class for table "tbl_log".
 *
 * The followings are the available columns in table 'tbl_log':
 * @property integer $id
 * @property string $ip
 * @property string $time
 * @property string $method
 * @property string $request
 * @property string $protocol
 * @property string $status
 * @property string $size
 * @property string $referer
 * @property string $user_agent
 * @property string $forwarded_for
 */
class Log extends CActiveRecord
{
    public $date_first;

    public $date_last;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_log';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ip, time, method, request, protocol, status, size, referer, user_agent, forwarded_for', 'required'),
            array('ip', 'length', 'max' => 32),
            array('method', 'length', 'max' => 8),
            array('request, referer, user_agent, forwarded_for', 'length', 'max' => 512),
            array('protocol', 'length', 'max' => 16),
            array('status, size', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ip, time, method, request, protocol, status, size, referer, user_agent, forwarded_for, date_first, date_last', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ip' => 'Ip',
            'time' => 'Time',
            'method' => 'Method',
            'request' => 'Request',
            'protocol' => 'Protocol',
            'status' => 'Status',
            'size' => 'Size',
            'referer' => 'Referer',
            'user_agent' => 'User Agent',
            'forwarded_for' => 'Forwarded For',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ip', $this->ip, true);

        if (isset($this->date_last) && trim($this->date_last) != "") {
            $c = date('Y-m-d H:i:s', strtotime($this->date_last));
            $criteria->addCondition("time < '{$c}'");
        }

        if ((isset($this->date_first) && trim($this->date_first) != "")) {
            $c = date('Y-m-d H:i:s', strtotime($this->date_first));
            $criteria->addCondition("time >= '{$c}'");
        }

        $criteria->compare('method', $this->method, true);
        $criteria->compare('request', $this->request, true);
        $criteria->compare('protocol', $this->protocol, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('size', $this->size, true);
        $criteria->compare('referer', $this->referer, true);
        $criteria->compare('user_agent', $this->user_agent, true);
        $criteria->compare('forwarded_for', $this->forwarded_for, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Log the static model class
     */
    public
    static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
